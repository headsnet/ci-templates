# Ci Templates

Headsnet CI template files.

## CI Includes

## CI Components

### Enforce Code Coverage

Queries code coverage reports using the Gitlab API. 

Failures are triggered via two criteria:
  - if the code coverage falls compared to the `main` branch.
  - if the code coverage is below the absolute minimum value.

| Input             | Type   | Default   |
|-------------------|--------|-----------|
| `absolute_minimum` | string | 75        |
| `branch`          | string | main      |
| `stage`           | string | post_test |
